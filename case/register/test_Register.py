# _*_ coding: UTF-8 _*_
# @Time     : 2021/12/22
# @Author   : zyz
# @Site     : zzyyzzzyz@qq.com

import unittest
import time
from parameterized import parameterized
# page导入
from page.RegisterPage import RegisterPage
from page.HomePage import HomePage
# lib导入
from InitWebDriver import InitWebDriver
from Random9Num import Random9Num
import logging


class Test_Register(unittest.TestCase):
    # 注册模块
    @classmethod
    def setUpClass(cls) -> None:
        # 打开网站首页
        cls.driver = InitWebDriver().returnWebDriver()
        # 进入注册页面
        homepage = HomePage(cls.driver)
        homepage.into_registerPage()
        time.sleep(5)

    @classmethod
    def tearDownClass(cls) -> None:
        time.sleep(5)
        cls.driver.quit()

    def setUp(self) -> None:
        time.sleep(5)

    def tearDown(self) -> None:
        self.driver.refresh()
        time.sleep(5)

    @parameterized.expand(
        [
            ('case1', 'test'+Random9Num().returnNum(), '123456', '注册成功'),
            ('case2', '1', '123456', '用户名格式由 字母数字下划线 2~18 个字符')
        ]
    )
    def test01(self, case, user, pwd, result):
        """
        :param case:case
        :param user:账号
        :param pwd:密码
        :param result:注册结果
        :return:None
        """
        registerpage = RegisterPage(self.driver)
        registerpage.input_account(user)
        registerpage.input_pwd(pwd)
        registerpage.agree()
        msg = registerpage.click_registerBtn()
        if msg == '注册成功':
            homepage = HomePage(self.driver)
            homepage.logout()
            homepage.into_registerPage()
        self.assertEqual(result, msg)


if __name__ == '__main__':
    unittest.main(verbosity=2)
























