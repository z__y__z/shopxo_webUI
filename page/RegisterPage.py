# _*_ coding: UTF-8 _*_
# @Time     : 2021/12/19
# @Author   : zyz
# @Site     : zzyyzzzyz@qq.com

from BasePage import BasePage
import time


class RegisterPage(BasePage):
    """
    注册页面page
    """
    def input_account(self, account):
        """
        输入注册的账号
        :param accout：注册的账号
        :return:None
        """
        self.by_name('accounts').send_keys(account)

    def input_pwd(self,pwd):
        """
        输入注册的密码
        :param pwd:密码
        :return:None
        """
        self.by_name('pwd').send_keys(pwd)

    def agree(self):
        """
        勾选协议
        :return:None
        """
        self.by_css('.form-validation-username .am-icon-checked').click()

    def click_registerBtn(self):
        """
        点击注册按钮
        :Return:返回注册成功或者失败的信息
        """
        self.by_name('pwd').submit()
        msg = self.by_css('#common-prompt p').text
        while msg == '':
            time.sleep(0.5)
            msg = self.by_css('#common-prompt p').text
        return msg
