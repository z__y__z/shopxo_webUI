# _*_ coding: UTF-8 _*_
# @Time     : 2021/12/19
# @Author   : zyz
# @Site     : zzyyzzzyz@qq.com

from ReadConfig import ReadConfig
from selenium import webdriver
from selenium.webdriver.chrome.service import Service as ChromeService
from selenium.webdriver.firefox.service import Service as FirefoxService


class InitWebDriver:
    """
    初始化webriver，并打开网站首页
    """

    def __init__(self):
        # 读取配置文件
        config = ReadConfig()
        # 读取配置项
        driver = config.get_config('driver')
        url = config.get_config('url')
        if 'chromedriver' in driver:
            s = ChromeService(driver)
            self.driver = webdriver.Chrome(service=s)
        if 'geckodriver' in driver:
            s = FirefoxService(driver)
            self.driver = webdriver.Firefox(service=s)
        self.driver.implicitly_wait(10)
        self.driver.get(url)

    def returnWebDriver(self):
        """
        返回webdriver对象
        :return: webdriver
        """
        return self.driver


if __name__ == "__main__":
    InitWebDriver().returnWebDriver()
