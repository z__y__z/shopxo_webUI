# _*_ coding: UTF-8 _*_
# @Time     : 2021/12/22
# @Author   : zyz
# @Site     : zzyyzzzyz@qq.com

import yagmail


class SendMail:
    def __init__(self, report):
        # 连接邮箱服务器
        yag = yagmail.SMTP(user="", password="", host='smtp.qq.com')
        # 主题
        subject = ['xx自动化测试报告']
        # 邮箱正文
        contents = ['领导，您好。附件为此轮自动化测试报告，请查看']
        # 发送邮件
        yag.send('', subject, contents, report)


if __name__ == '__main__':
    SendMail(r'')
